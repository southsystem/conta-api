package br.workshop.conta.api.service;

import br.workshop.conta.api.config.OutputEvents;
import br.workshop.conta.api.integration.limit.ContaCriadaEvent;
import br.workshop.conta.api.model.dto.CreateContaRequest;
import br.workshop.conta.api.model.entity.Agencia;
import br.workshop.conta.api.model.entity.Conta;
import br.workshop.conta.api.repository.ContaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public class ContaService {

    private final ContaRepository repository;
    private final AgenciaService agenciaService;
    private final OutputEvents outputEvents;

    public ContaService(ContaRepository repository, AgenciaService agenciaService, OutputEvents outputEvents) {
        this.repository = repository;
        this.agenciaService = agenciaService;
        this.outputEvents = outputEvents;
    }

    public Conta create(CreateContaRequest request) throws Exception {
        Agencia agencia = agenciaService.getAgencia(request.getAgencia(), request.getCnpjRaiz());
        Conta conta = new Conta(request, agencia);

        conta = repository.save(conta);

        sendCreateAccountEvent(conta);

        return conta;
    }

    private void sendCreateAccountEvent(Conta conta) {
        ContaCriadaEvent event = new ContaCriadaEvent(conta.getId(), conta.getTipo());

        outputEvents.sendCreateAccount()
                .send(MessageBuilder.withPayload(event)
                        .build());
    }

    public Page<Conta> getAll(Pageable pageable) {
        return repository.findAll(pageable);
    }
}
