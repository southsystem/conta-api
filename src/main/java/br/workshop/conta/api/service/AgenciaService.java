package br.workshop.conta.api.service;

import br.workshop.conta.api.integration.bcb.BcbResponse;
import br.workshop.conta.api.integration.bcb.InformesAgencia;
import br.workshop.conta.api.model.entity.Agencia;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AgenciaService {

    private RestTemplate restTemplate;

    @Value("${bcb.agencia.url}")
    private String url;

    public AgenciaService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Agencia getAgencia(String agencia, String cnpjRaiz) throws Exception {
        ResponseEntity<BcbResponse> response = restTemplate.getForEntity(buildUrlFilter(agencia, cnpjRaiz), BcbResponse.class);

        InformesAgencia agenciaResponse = response.getBody().getValue().stream().findFirst()
                .orElseThrow(() -> new Exception("Agencia não encontrada"));

        return new Agencia(agenciaResponse);
    }

    private String buildUrlFilter(String agencia, String cnpjRaiz) {
        return url + "?$format=json&$filter=CodigoCompe eq '" + agencia
                + "' and CnpjBase eq '" + cnpjRaiz
                + "'&$skip=0&$top=1";
    }
}
