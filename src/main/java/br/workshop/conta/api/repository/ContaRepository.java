package br.workshop.conta.api.repository;

import br.workshop.conta.api.model.entity.Conta;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContaRepository extends PagingAndSortingRepository<Conta, String> {

    Page<Conta> findByNumero(String numero, Pageable pageable);
}
