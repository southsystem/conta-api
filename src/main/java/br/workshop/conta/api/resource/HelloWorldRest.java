package br.workshop.conta.api.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HelloWorldRest {

    @GetMapping
    public String hello() {
        return "Hello World";
    }

    @PostMapping
    public String helloPost() {
        return "Hello post";
    }
}
