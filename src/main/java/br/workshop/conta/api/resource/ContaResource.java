package br.workshop.conta.api.resource;

import br.workshop.conta.api.model.dto.CreateContaRequest;
import br.workshop.conta.api.model.entity.Conta;
import br.workshop.conta.api.service.ContaService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/conta")
public class ContaResource {

    private final ContaService service;

    public ContaResource(ContaService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<Page<Conta>> getAll(Pageable pageable) {
        return ResponseEntity.ok(service.getAll(pageable));
    }

    @PostMapping
    public ResponseEntity<Conta> create(@RequestBody @Valid CreateContaRequest request) throws Exception {
        return ResponseEntity.ok(service.create(request));
    }
}
