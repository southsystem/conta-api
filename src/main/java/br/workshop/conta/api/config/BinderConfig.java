package br.workshop.conta.api.config;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding({OutputEvents.class})
public class BinderConfig {

}
