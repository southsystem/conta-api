package br.workshop.conta.api.config;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

@Component
public interface OutputEvents {

    String createAccountEvent = "CREATE_ACCOUNT_EVENT";

    @Output(OutputEvents.createAccountEvent)
    MessageChannel sendCreateAccount();
}
