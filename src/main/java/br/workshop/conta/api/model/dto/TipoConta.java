package br.workshop.conta.api.model.dto;

public enum TipoConta {
    PF, PJ;
}
