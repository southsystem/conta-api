package br.workshop.conta.api.model.entity;

import br.workshop.conta.api.model.dto.CreateContaRequest;
import br.workshop.conta.api.model.dto.TipoConta;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Conta {

    @Id
    private String id;
    private String numero;
    private String cpf;
    private TipoConta tipo;
    private Agencia agencia;

    public Conta(){

    }

    public Conta(CreateContaRequest request, Agencia agencia) {
        this.numero = request.getNumero();
        this.cpf = request.getCpf();
        this.tipo = request.getTipo();
        this.agencia = agencia;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public TipoConta getTipo() {
        return tipo;
    }

    public void setTipo(TipoConta tipo) {
        this.tipo = tipo;
    }
}
