package br.workshop.conta.api.model.entity;

import br.workshop.conta.api.integration.bcb.InformesAgencia;

import java.time.LocalDate;

public class Agencia {

    private String cnpjBase;
    private String cnpjSequencial;
    private Long cnpjDv;
    private String nomeIf;
    private String segmento;
    private String codigoCompe;
    private String nomeAgencia;
    private String endereco;
    private String numero;
    private String complemento;
    private String bairro;
    private String cep;
    private String municipioIbge;
    private String municipio;
    private String uf;
    private LocalDate dataInicio;
    private Long ddd;
    private Long telefone;
    private LocalDate posicao;

    public Agencia() {}
    public Agencia(InformesAgencia agenciaResponse) {
        this.bairro = agenciaResponse.getBairro();
        this.cep = agenciaResponse.getCep();
        this.cnpjBase = agenciaResponse.getCnpjBase();
        this.cnpjDv = agenciaResponse.getCnpjDv();
        this.cnpjSequencial = agenciaResponse.getCnpjSequencial();
        this.codigoCompe = agenciaResponse.getCodigoCompe();
        this.complemento = agenciaResponse.getComplemento();
        this.dataInicio = agenciaResponse.getDataInicio();
        this.ddd = agenciaResponse.getDdd();
        this.endereco = agenciaResponse.getEndereco();
        this.municipio = agenciaResponse.getMunicipio();
        this.municipioIbge = agenciaResponse.getMunicipioIbge();
        this.nomeAgencia = agenciaResponse.getNomeAgencia();
        this.nomeIf = agenciaResponse.getNomeIf();
        this.numero = agenciaResponse.getNumero();
        this.posicao = agenciaResponse.getPosicao();
        this.segmento = agenciaResponse.getSegmento();
        this.telefone = agenciaResponse.getTelefone();
        this.uf = agenciaResponse.getUf();
    }

    public String getCnpjBase() {
        return cnpjBase;
    }

    public void setCnpjBase(String cnpjBase) {
        this.cnpjBase = cnpjBase;
    }

    public String getCnpjSequencial() {
        return cnpjSequencial;
    }

    public void setCnpjSequencial(String cnpjSequencial) {
        this.cnpjSequencial = cnpjSequencial;
    }

    public Long getCnpjDv() {
        return cnpjDv;
    }

    public void setCnpjDv(Long cnpjDv) {
        this.cnpjDv = cnpjDv;
    }

    public String getNomeIf() {
        return nomeIf;
    }

    public void setNomeIf(String nomeIf) {
        this.nomeIf = nomeIf;
    }

    public String getSegmento() {
        return segmento;
    }

    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    public String getCodigoCompe() {
        return codigoCompe;
    }

    public void setCodigoCompe(String codigoCompe) {
        this.codigoCompe = codigoCompe;
    }

    public String getNomeAgencia() {
        return nomeAgencia;
    }

    public void setNomeAgencia(String nomeAgencia) {
        this.nomeAgencia = nomeAgencia;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getMunicipioIbge() {
        return municipioIbge;
    }

    public void setMunicipioIbge(String municipioIbge) {
        this.municipioIbge = municipioIbge;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public LocalDate getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(LocalDate dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Long getDdd() {
        return ddd;
    }

    public void setDdd(Long ddd) {
        this.ddd = ddd;
    }

    public Long getTelefone() {
        return telefone;
    }

    public void setTelefone(Long telefone) {
        this.telefone = telefone;
    }

    public LocalDate getPosicao() {
        return posicao;
    }

    public void setPosicao(LocalDate posicao) {
        this.posicao = posicao;
    }
}
