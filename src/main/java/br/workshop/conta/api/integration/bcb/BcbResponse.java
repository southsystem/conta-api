package br.workshop.conta.api.integration.bcb;

import java.util.ArrayList;
import java.util.List;

public class BcbResponse {

    private List<InformesAgencia> value;

    public BcbResponse() {
        this.value = new ArrayList<>();
    }

    public List<InformesAgencia> getValue() {
        return value;
    }

    public void setValue(List<InformesAgencia> value) {
        this.value = value;
    }
}
