package br.workshop.conta.api.integration.limit;

import br.workshop.conta.api.model.dto.TipoConta;

public class ContaCriadaEvent {

    private String idConta;
    private TipoConta tipo;

    public ContaCriadaEvent(String id, TipoConta tipo) {
        this.idConta = id;
        this.tipo = tipo;
    }

    public String getIdConta() {
        return idConta;
    }

    public void setIdConta(String idConta) {
        this.idConta = idConta;
    }

    public TipoConta getTipo() {
        return tipo;
    }

    public void setTipo(TipoConta tipo) {
        this.tipo = tipo;
    }
}
