package br.workshop.conta.api.integration.bcb;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDate;

public class InformesAgencia implements Serializable {

    @JsonProperty("CnpjBase")
    private String cnpjBase;
    @JsonProperty("CnpjSequencial")
    private String cnpjSequencial;
    @JsonProperty("CnpjDv")
    private Long cnpjDv;
    @JsonProperty("NomeIf")
    private String nomeIf;
    @JsonProperty("Segmento")
    private String segmento;
    @JsonProperty("CodigoCompe")
    private String codigoCompe;
    @JsonProperty("NomeAgencia")
    private String nomeAgencia;
    @JsonProperty("Endereco")
    private String endereco;
    @JsonProperty("Numero")
    private String numero;
    @JsonProperty("Complemento")
    private String complemento;
    @JsonProperty("Bairro")
    private String bairro;
    @JsonProperty("Cep")
    private String cep;
    @JsonProperty("MunicipioIbge")
    private String municipioIbge;
    @JsonProperty("Municipio")
    private String municipio;
    @JsonProperty("UF")
    private String uf;
    @JsonProperty("DataInicio")
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dataInicio;
    @JsonProperty("DDD")
    private Long ddd;
    @JsonProperty("Telefone")
    private Long telefone;
    @JsonProperty("Posicao")
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate posicao;

    public String getCnpjBase() {
        return cnpjBase;
    }

    public void setCnpjBase(String cnpjBase) {
        this.cnpjBase = cnpjBase;
    }

    public String getCnpjSequencial() {
        return cnpjSequencial;
    }

    public void setCnpjSequencial(String CnpjSequencial) {
        this.cnpjSequencial = CnpjSequencial;
    }

    public Long getCnpjDv() {
        return cnpjDv;
    }

    public void setCnpjDv(Long cnpjDv) {
        this.cnpjDv = cnpjDv;
    }

    public String getNomeIf() {
        return nomeIf;
    }

    public void setNomeIf(String nomeIf) {
        this.nomeIf = nomeIf;
    }

    public String getSegmento() {
        return segmento;
    }

    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    public String getCodigoCompe() {
        return codigoCompe;
    }

    public void setCodigoCompe(String codigoCompe) {
        this.codigoCompe = codigoCompe;
    }

    public String getNomeAgencia() {
        return nomeAgencia;
    }

    public void setNomeAgencia(String nomeAgencia) {
        this.nomeAgencia = nomeAgencia;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getMunicipioIbge() {
        return municipioIbge;
    }

    public void setMunicipioIbge(String municipioIbge) {
        this.municipioIbge = municipioIbge;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public LocalDate getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(LocalDate dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Long getDdd() {
        return ddd;
    }

    public void setDdd(Long ddd) {
        this.ddd = ddd;
    }

    public Long getTelefone() {
        return telefone;
    }

    public void setTelefone(Long telefone) {
        this.telefone = telefone;
    }

    public LocalDate getPosicao() {
        return posicao;
    }

    public void setPosicao(LocalDate posicao) {
        this.posicao = posicao;
    }
}
